import pyo3_example
import time
from threading import Thread
import asyncio
from ctypes import cdll
from sys import platform
import sys
import numpy as np
from enum import Enum
import json

class Color(Enum):
    TRAIN = 1
    AGGREGATE = 2

def convert_numpy_to_list(obj):
    if isinstance(obj, np.ndarray):
        return obj.tolist()
    elif isinstance(obj, dict):
        return {key: convert_numpy_to_list(value) for key, value in obj.items()}
    elif isinstance(obj, list):
        return [convert_numpy_to_list(element) for element in obj]
    else:
        return obj

class Node(Thread):
    def __init__(self, address: str, port: int, topic: str, tcp: bool =True, udp: bool =False):
        super().__init__()
        self.daemon = True
        self.address = address
        self.port= port
        self.topic=topic
        self.tcp=tcp
        self.udp=udp

        # self.network = pyo3_example.Network(address, port, tcp, udp, topic)
        self.network = pyo3_example.Net()

        hash_set = set()
        hash_set.add(topic)

        self.topics = hash_set
    
    def send_message(self, topic: str, message: str):
        self.network.send_message(topic, message)
    
    def get_listening_address(self):
        res = self.network.get_listening_address()
        
        return res
    
    def get_messages(self) -> set:
        res = self.network.get_messages()
        
        return res
    
    def get_discovered_peers(self) -> set:
        res = self.network.get_discovered_peers()

        return res
    
    def get_peer_id(self) -> str:
        res = self.network.get_peer_id()

        return res
    
    def get_connected_peers(self) -> set:
        res = self.network.get_connected_peers()

        return res
    
    def subscribe_topic(self, topic: str):
        self.network.subscribe_topic(topic)
        self.topics.add(topic)

    async def future_get_message(self):
        await self.network.future_get_message()
    
    async def started(self):
        await self.network.create_start(self.address, self.port, self.tcp, self.udp, self.topic, None)

    def run(self) -> None:
        # print("started")
        asyncio.run(self.started())
        # self.network.create_start(self.address, self.port, self.tcp, self.udp, self.topic)

if __name__ == '__main__':
    print("Create Node")

    node = Node(address="0.0.0.0", port=0, topic="test-net", tcp=True, udp=True)

    print("start node")
    node.start()

    print("Node started")

    print("Create thread for getting messages")
    thread = Thread(target=asyncio.run, args=(node.future_get_message(),), daemon=True)
    print("Start thread for getting messages")
    thread.start()

    # print("Create thread for getting messages")
    # thread1 = Thread(target=asyncio.run, args=(node.future_get_message(),), daemon=True)
    # print("Start thread for getting messages")
    # thread1.start()

    # node.network.run()


    time.sleep(1)

    a = 0

    peer_id = node.get_peer_id()

    vecteur = np.array([1, 2, 3])

    # Définir une matrice (un tableau bidimensionnel)
    matrice = np.array([[4, 5, 6],
                    [7, 8, 9],
                   [10, 11, 12]])

    vecteur = np.dot(vecteur, matrice)

    list_topic = list(node.topics)
    topic = list_topic[0]
    
    while True:
        # print("Get discovered peers")
        conn = node.get_connected_peers()
        # print(discov)

        if conn :

            if a == 0 :
                data = {
                    "id": peer_id,
                    "data": convert_numpy_to_list(vecteur)
                }

                # data = convert_numpy_to_list
                data_string = json.dumps(data)
                node.send_message(topic=topic, message=data_string)
            # print("Sending message")

            # list_topic = list(node.topics)
        
        # print("Get message")
        messages = node.get_messages()
        if messages:
            for msg in messages:
                data = json.loads(msg.message)

                data["id"] = peer_id
                if a % 5 == 0:
                    data["data"] = convert_numpy_to_list(np.dot(np.array(data["data"]), matrice))
                    vecteur = data["data"]
                    node.send_message(topic=topic, message=json.dumps(data))
                else:
                    data["data"] = convert_numpy_to_list(np.mean([vecteur, np.array(data["data"])], axis=0))
                    vecteur = convert_numpy_to_list(data["data"])
                    node.send_message(topic=topic, message=json.dumps(data))
        
        print(vecteur)
        a = a+1

        # time.sleep(5)