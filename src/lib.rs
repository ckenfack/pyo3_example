use futures::channel::{mpsc, oneshot};
use futures::{prelude::*, SinkExt};
use libp2p::{
    futures::StreamExt,
    gossipsub::{self, MessageId, TopicHash},
    mdns, noise,
    swarm::{NetworkBehaviour, SwarmEvent},
    tcp, yamux, PeerId, Swarm,
};
use pyo3::prelude::*;
use std::collections::HashMap;
use std::{
    collections::{hash_map::DefaultHasher, HashSet},
    error::Error,
    hash::{Hash, Hasher},
    sync::Arc,
    time::Duration,
};
use tokio::runtime::Handle;
use tokio::{io, spawn};
use tracing::event;
use tracing_subscriber::EnvFilter;

#[derive(NetworkBehaviour)]
pub struct MyBehaviour {
    pub gossipsub: gossipsub::Behaviour,
    mdns: mdns::tokio::Behaviour,
}

#[derive(Clone, Debug)]
#[pyclass(name = "Net", subclass)]
pub struct Net {
    sender: mpsc::Sender<Command>,
    topics: HashMap<String, TopicHash>,
    command_receiver: Arc<tokio::sync::Mutex<mpsc::Receiver<Command>>>,
    event_sender: mpsc::Sender<Event>,
    event_receiver: Arc<tokio::sync::Mutex<mpsc::Receiver<Event>>>,
    messages: HashSet<Data>,
    request_sender: mpsc::Sender<Request>,
    request_receiver: Arc<tokio::sync::Mutex<mpsc::Receiver<Request>>>,
}
// #[derive(Clone)]
#[pyclass(name = "Node", subclass)]
pub struct Node {
    pub id: PeerId,
    pub message: String,
    #[pyo3(get, set)]
    pub connected_peers: HashSet<String>,
    #[pyo3(get, set)]
    pub discovered_peers: HashSet<String>,
    #[pyo3(get, set)]
    pub listening_address: HashSet<String>,
    pub swarm: Swarm<MyBehaviour>,
    command_receiver: Arc<tokio::sync::Mutex<mpsc::Receiver<Command>>>,
    event_sender: mpsc::Sender<Event>,
}

// #[derive(Clone)]
#[pyclass(name = "Client", subclass)]
pub struct Client {
    id: PeerId,
    sender: mpsc::Sender<Command>,
    topics: HashMap<String, TopicHash>,
}

#[pymodule]
fn pyo3_example(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<Node>()?;
    m.add_class::<Client>()?;
    m.add_class::<Net>()?;
    m.add_class::<Data>()?;
    Ok(())
}

#[pymethods]
impl Net {
    #[new]
    fn new() -> Self {
        let (command_sender, command_receiver) = mpsc::channel(10);
        let (event_sender, event_receiver) = mpsc::channel(1);
        let (request_sender, request_receiver) = mpsc::channel(10);

        Self {
            sender: command_sender,
            topics: HashMap::new(),
            command_receiver: Arc::new(tokio::sync::Mutex::new(command_receiver)),
            event_sender,
            event_receiver: Arc::new(tokio::sync::Mutex::new(event_receiver)),
            messages: HashSet::new(),
            request_sender,
            request_receiver: Arc::new(tokio::sync::Mutex::new(request_receiver))
        }
    }

    fn create_start<'a>(
        &mut self,
        address: Option<&str>,
        port: Option<u32>,
        tcp: Option<bool>,
        udp: Option<bool>,
        msg_topic: Option<&str>,
        max_transmit: Option<usize>,
        py: Python<'a>,
    ) -> PyResult<&'a PyAny> {
        let topic = gossipsub::IdentTopic::new(msg_topic.unwrap());
        self.topics
            .insert(msg_topic.unwrap().to_string(), topic.into());

        let network = self.clone();

        pyo3_asyncio::tokio::future_into_py(
            py,
            start(
                network,
                address.unwrap().to_string(),
                port,
                tcp,
                udp,
                msg_topic.unwrap().to_string(),
                max_transmit
            ),
        )
    }

    fn future_get_message<'a>(
        &mut self,
        py: Python<'a>,
    ) -> PyResult<&'a PyAny> {

        let network = self.clone();

        pyo3_asyncio::tokio::future_into_py(
            py,
            network.async_get_message()
        )
    }



    pub fn get_discovered_peers(&mut self) -> HashSet<String> {
        let rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async { self.async_get_discovered_peers().await })
    }

    pub fn get_peer_id(&mut self) -> String {
        let rt = tokio::runtime::Runtime::new().unwrap();
        let res = rt.block_on(async { self.async_get_peer_id().await });

        res.to_string()
    }

    fn send_message(&mut self, topic: String, msg: String) {
        let rt = tokio::runtime::Runtime::new().unwrap();
        let _ = rt.block_on(async { self.async_send_message(topic, msg).await });
    }

    fn subscribe_topic(&mut self, topic: String) {
        let rt = tokio::runtime::Runtime::new().unwrap();
        let _ = rt.block_on(async { self.async_subscribe_topic(topic).await });
    }

    fn unsubscribe_topic(&mut self, topic: String) {
        let rt = tokio::runtime::Runtime::new().unwrap();
        let _ = rt.block_on(async { self.async_unsubscribe_topic(topic).await });
    }

    pub fn get_connected_peers(&mut self) -> HashSet<String> {
        let rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async { self.async_get_connected_peers().await })
    }

    pub fn get_listening_address(&mut self) -> HashSet<String> {
        let rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async { self.async_get_listening_address().await })
    }

    pub fn get_messages(&mut self) -> HashSet<Data> {
        let rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async { self.async_request_get_messages().await })
    }
}

async fn start<'a>(
    network: Net,
    address: String,
    port: Option<u32>,
    tcp: Option<bool>,
    udp: Option<bool>,
    msg_topic: String,
    max_transmit: Option<usize>
) -> PyResult<()> {
    let mut net = network;
    // println!("Create node");

    let (topic, mut node) = create_node_net(
        Some(&address),
        port,
        tcp,
        udp,
        Some(&msg_topic),
        max_transmit,
        Arc::clone(&net.command_receiver),
        net.event_sender.clone(),
    )
    .await
    .expect("Failed to create Node");

    // println!("Save the topic");

    net.topics.insert(msg_topic, topic.clone());

    // println!("Run node in background");

    let handle = spawn(async move { async_run(&mut node).await });
    let _ = handle.await;
    Ok(())
}

impl Net {
    /// Subscribe to a new topic
    async fn async_subscribe_topic(&mut self, topic: String) -> Result<(), Box<dyn Error + Send>> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::SubscribeTopic { topic, sender })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.")
    }

    /// Unsubscribe to a topic
    async fn async_unsubscribe_topic(
        &mut self,
        topic: String,
    ) -> Result<(), Box<dyn Error + Send>> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::UnSubscribeTopic { topic, sender })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.")
    }

    /// Get the list of connected peers
    async fn async_get_connected_peers(&mut self) -> HashSet<String> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::GetConnectedPeers { sender })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.")
    }

    /// Get the list of discovered peers
    async fn async_get_discovered_peers(&mut self) -> HashSet<String> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::GetDiscoveredPeers { sender })
            .await
            .expect("Command receiver not to be dropped.");
        let res = receiver.await.expect("Sender not to be dropped.");

        for element in &res {
            let peer_id = peer_id_from_str(element.as_str());
            self.sender
                .send(Command::ConnectPeer { peer_id })
                .await
                .expect("Error");
        }

        res
    }

    /// Get listening address
    async fn async_get_listening_address(&mut self) -> HashSet<String> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::ListeningAddress { sender })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.")
    }

    /// Get peer id
    async fn async_get_peer_id(&mut self) -> PeerId {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::GetPeerId { sender })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.")
    }

    /// Request to get the received messages
    async fn async_request_get_messages(&mut self) -> HashSet<Data> {
        let (sender, receiver) = oneshot::channel();
        self.request_sender
            .send(Request::GetMessage { sender })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.")
    }

    /// Send message
    async fn async_send_message(&mut self, topic: String, message: String) {
        let topic_hash = self.topics.get(&topic);
        match topic_hash {
            Some(val) => self
                .sender
                .send(Command::SendToOne {
                    topic: val.clone(),
                    message,
                })
                .await
                .expect("Command receiver not to be dropped"),
            None => {
                // println!("not found")
            }
        }
    }

    async fn async_get_message(self) -> PyResult<()>{

        let mut net = self;

        let event_arc_clone = Arc::clone(&net.event_receiver);
        let mut event_guard = event_arc_clone.lock().await;
        let event_receiver = &mut *event_guard;

        let request_arc_clone = Arc::clone(&net.request_receiver);
        let mut request_guard = request_arc_clone.lock().await;
        let request_receiver = &mut *request_guard;

        loop {
            futures::select! {
                event = event_receiver.next() => match event {
                    Some(e) => {
                        handle_event(e, &mut net).await
                    },
                    None => todo!()
                },
                request = request_receiver.next() => match request {
                    Some(req) => {
                        handle_request(req, &mut net).await
                    },
                    None => todo!()
                }
            }
        }
    }
}

async fn handle_event(event: Event, network: &mut Net){
    match event {
        Event::InCommingMessage { propagation_source, message_id, topic, message } => {
            network.messages.insert(Data {
                propagation_source: propagation_source.to_string(), message_id: message_id.to_string(), topic: topic.to_string(), message
            });
        },
    }
}

async fn handle_request(request: Request, network: &mut Net){
    match request {
        Request::GetMessage { sender } => {
            let mut messages = HashSet::new();
            network.messages.clone().into_iter().for_each(|msg| {
                messages.insert(msg);
            });
            network.messages.clear();
            sender.send(messages).expect("Error sending message");
        },
    }
}

async fn create_node_net(
    address: Option<&str>,
    port: Option<u32>,
    tcp: Option<bool>,
    udp: Option<bool>,
    msg_topic: Option<&str>,
    max_transmit: Option<usize>,
    command_receiver: Arc<tokio::sync::Mutex<mpsc::Receiver<Command>>>,
    event_sender: mpsc::Sender<Event>,
) -> Result<(TopicHash, Node), Box<dyn Error>> {
    let addr: &str = match address {
        Some(val) => val,
        None => "0.0.0.0",
    };

    let po = match port {
        Some(val) => val,
        None => 0,
    };

    let max_transmit_size: usize = match max_transmit {
        Some(val) => val,
        None => 2048
    };

    println!("max_transmit_size: {}", max_transmit_size);

    let _ = tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .try_init();

    let mut swarm = libp2p::SwarmBuilder::with_new_identity()
        .with_tokio()
        .with_tcp(
            tcp::Config::default().port_reuse(true).nodelay(true),
            noise::Config::new,
            yamux::Config::default,
        )
        .unwrap()
        .with_quic()
        .with_behaviour(|key| {
            // To content-address message, we can take the hash of message and use it as an ID.
            let message_id_fn = |message: &gossipsub::Message| {
                let mut s = DefaultHasher::new();
                message.data.hash(&mut s);
                gossipsub::MessageId::from(s.finish().to_string())
            };

            // let max_transmit_size:usize = 2048*2048*2048;
            // Set a custom gossipsub configuration
            let gossipsub_config = gossipsub::ConfigBuilder::default()
                .heartbeat_interval(Duration::from_secs(10)) // This is set to aid debugging by not cluttering the log space
                .validation_mode(gossipsub::ValidationMode::Strict) // This sets the kind of message validation. The default is Strict (enforce message signing)
                .message_id_fn(message_id_fn) // content-address messages. No two messages of the same content will be propagated.
                .max_transmit_size(max_transmit_size)
                .build()
                .map_err(|msg| io::Error::new(io::ErrorKind::Other, msg))?; // Temporary hack because `build` does not return a proper `std::error::Error`.

            // build a gossipsub network behaviour
            let gossipsub = gossipsub::Behaviour::new(
                gossipsub::MessageAuthenticity::Signed(key.clone()),
                gossipsub_config,
            )?;

            let mdns =
                mdns::tokio::Behaviour::new(mdns::Config::default(), key.public().to_peer_id())?;
            Ok(MyBehaviour { gossipsub, mdns })
        })
        .unwrap()
        .with_swarm_config(|c| c.with_idle_connection_timeout(Duration::from_secs(60)))
        .build();

    // Create a Gossipsub topic
    let topic = gossipsub::IdentTopic::new(msg_topic.unwrap());
    // subscribes to our topic
    swarm.behaviour_mut().gossipsub.subscribe(&topic).unwrap();

    // Listen on all interfaces and whatever port the OS assigns
    if tcp.unwrap() {
        let tcp_address = format!("/ip4/{}/tcp/{}", addr, po);
        swarm
            .listen_on(tcp_address.to_owned().parse().unwrap())
            .unwrap();
    }
    if udp.unwrap() {
        let udp_address = format!("/ip4/{}/udp/{}/quic-v1", addr, po);
        swarm
            .listen_on(udp_address.to_owned().parse().unwrap())
            .unwrap();
    } else if !tcp.unwrap() {
        let tcp_address = format!("/ip4/{}/tcp/{}", addr, po);
        swarm
            .listen_on(tcp_address.to_owned().parse().unwrap())
            .unwrap();
    }

    let id = *swarm.local_peer_id();

    let node = Node {
        id,
        message: String::new(),
        connected_peers: HashSet::new(),
        swarm,
        command_receiver,
        event_sender,
        discovered_peers: HashSet::new(),
        listening_address: HashSet::new(),
    };

    Ok((topic.into(), node))
}

pub async fn async_run(node: &mut Node) {

    let command_arc_clone = Arc::clone(&node.command_receiver);
    let mut command_guard = command_arc_clone.lock().await;
    let command_receiver = &mut *command_guard;

    loop {
        // let evt = {
        futures::select! {
            event = node.swarm.next() => handle_swarm_event(&mut node.event_sender, event.expect("Swarm stream to be infinite."), &mut node.swarm).await,
            command = command_receiver.next() => match command {
                Some(c) => {
                    handle_command(c, &mut node.swarm).await},
                // Command channel closed, thus shutting down the network event loop.
                None=>  {},
            },
        };
    }
}

async fn handle_swarm_event(
    event_sender: &mut mpsc::Sender<Event>,
    event: SwarmEvent<MyBehaviourEvent>,
    swarm: &mut Swarm<MyBehaviour>,
) {
    match event {
        SwarmEvent::Behaviour(MyBehaviourEvent::Gossipsub(gossipsub::Event::Message {
            propagation_source: peer_id,
            message_id: id,
            message,
        })) => {
            event_sender
                .send(Event::InCommingMessage {
                    propagation_source: peer_id,
                    message_id: id,
                    topic: message.topic,
                    message: String::from_utf8_lossy(&message.data).to_string(),
                })
                .await
                .expect("Error sending message");
        }
        SwarmEvent::Behaviour(MyBehaviourEvent::Mdns(mdns::Event::Discovered(list))) => {
            for (peer_id, _multiaddr) in list {
                // println!("mDNS discover peer has discovered: {}", peer_id);
                swarm.behaviour_mut().gossipsub.add_explicit_peer(&peer_id);
            }
        }
        SwarmEvent::Behaviour(MyBehaviourEvent::Mdns(mdns::Event::Expired(list))) => {
            for (peer_id, _multiaddr) in list {
                // println!("mDNS discover peer has expired: {}", peer_id);
                swarm
                    .behaviour_mut()
                    .gossipsub
                    .remove_explicit_peer(&peer_id);
            }
        }
        SwarmEvent::ConnectionEstablished {
            peer_id,
            connection_id,
            endpoint,
            num_established,
            concurrent_dial_errors,
            established_in,
        } => {
            // println!("Connection Established: {}", peer_id);
        }
        SwarmEvent::ConnectionClosed {
            peer_id,
            connection_id,
            endpoint,
            num_established,
            cause,
        } => {
            // println!("{}", cause.unwrap().to_string());
        }
        SwarmEvent::IncomingConnection {
            connection_id,
            local_addr,
            send_back_addr,
        } => {
            // println!("In comming connection: {}", connection_id);
        }
        SwarmEvent::IncomingConnectionError {
            connection_id,
            local_addr,
            send_back_addr,
            error,
        } => {
            // println!(
            //     "In comming connection error: {} error {}",
            //     connection_id, error
            // );
        }
        SwarmEvent::OutgoingConnectionError {
            connection_id,
            peer_id,
            error,
        } => {
            // println!(
                // "Out going connection error: {} error {}",
            //     connection_id, error
            // );
        }
        SwarmEvent::NewListenAddr {
            listener_id,
            address,
        } => {
            // println!("new listen address: {}", address);
        }
        SwarmEvent::ExpiredListenAddr {
            listener_id,
            address,
        } => {
            // println!("expired listen address: {}", address);
        }
        SwarmEvent::ListenerClosed {
            listener_id,
            addresses,
            reason,
        } => {
            // println!("Listener closed: {} ", listener_id);
            // addresses.iter().for_each(|f| // println!("{}", f))
        }
        SwarmEvent::ListenerError { listener_id, error } => {
            // println!("Listener error: {} error {}", listener_id, error);
        }
        SwarmEvent::Dialing {
            peer_id,
            connection_id,
        } => {
            // println!(
            //     "Dialing peer: {} with connection: {}",
            //     peer_id.unwrap(),
            //     connection_id
            // );
        }
        SwarmEvent::NewExternalAddrCandidate { address } => {
            // println!("new external address candidate: {}", address);
        }
        SwarmEvent::ExternalAddrConfirmed { address } => {
            // println!("external address confirmed: {}", address);
        }
        SwarmEvent::ExternalAddrExpired { address } => {
            // println!("external address expired: {}", address);
        }
        _ => {}
    }
}

async fn handle_command(
    // node: &mut Node,
    command: Command,
    swarm: &mut Swarm<MyBehaviour>,
) {
    match command {
        Command::SubscribeTopic { topic, sender } => {
            let topic = gossipsub::IdentTopic::new(topic.as_str());
            // let mut swarm = node.swarm.lock().await;
            swarm.behaviour_mut().gossipsub.subscribe(&topic).unwrap();
            // node.topics.insert(topic.into());
        }
        Command::UnSubscribeTopic { topic, sender } => {
            let topic = gossipsub::IdentTopic::new(topic.as_str());
            // let mut swarm = node.swarm.lock().await;
            let _ = swarm.behaviour_mut().gossipsub.unsubscribe(&topic).unwrap();
            // node.topics.remove(&topic.into());
        }
        Command::GetConnectedPeers { sender } => {
            let mut list = HashSet::new();
            swarm.behaviour().gossipsub.all_peers().for_each(|f| {
                list.insert((*f.0).to_string());
            });
            sender.send(list).unwrap();
        }
        Command::GetDiscoveredPeers { sender } => {
            // // println!("Receive Command::GetDiscoveredPeers");
            let mut list = HashSet::new();
            let nodes = swarm.behaviour().mdns.discovered_nodes();
            for peer in nodes {
                list.insert((*peer).to_string());
            }
            sender.send(list).unwrap();
        }
        Command::ListeningAddress { sender } => {
            let mut list = HashSet::new();
            swarm.listeners().for_each(|f| {
                list.insert(f.to_string());
            });
            sender.send(list).unwrap();
        }
        Command::SendToOne { topic, message } => {
            // // println!("Connect Peers: {}", peers.len());
            // for peer_id in peers {
            //     swarm.behaviour_mut().gossipsub.add_explicit_peer(&peer_id);
            // }
            // // println!("Message : {}", message);
            if let Err(e) = swarm
                .behaviour_mut()
                .gossipsub
                .publish(topic, message.as_bytes())
            {
                // // println!("Publish error: {e:?}");
            }
        }
        Command::ConnectPeer { peer_id } => {
            swarm.behaviour_mut().gossipsub.add_explicit_peer(&peer_id);
        }
        Command::GetPeerId { sender } => {
            let peer_id = *swarm.local_peer_id();
            sender.send(peer_id).unwrap();
        }
    }
}

#[derive(Debug)]
enum Command {
    SubscribeTopic {
        topic: String,
        sender: oneshot::Sender<Result<(), Box<dyn Error + Send>>>,
    },
    UnSubscribeTopic {
        topic: String,
        sender: oneshot::Sender<Result<(), Box<dyn Error + Send>>>,
    },
    SendToOne {
        topic: TopicHash,
        message: String,
    },
    GetConnectedPeers {
        sender: oneshot::Sender<HashSet<String>>,
    },
    GetDiscoveredPeers {
        sender: oneshot::Sender<HashSet<String>>,
    },
    ListeningAddress {
        sender: oneshot::Sender<HashSet<String>>,
    },
    ConnectPeer {
        peer_id: PeerId,
    },
    GetPeerId {
        sender: oneshot::Sender<PeerId>,
    },
}

#[derive(Debug)]
enum Request {
    GetMessage {
        sender: oneshot::Sender<HashSet<Data>>,
    },
}

enum Event {
    InCommingMessage {
        propagation_source: PeerId,
        message_id: MessageId,
        topic: TopicHash,
        message: String,
    },
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[pyclass(name = "Data", subclass)]
pub struct Data {
    #[pyo3(get, set)]
    propagation_source: String,
    #[pyo3(get, set)]
    message_id: String,
    #[pyo3(get, set)]
    topic: String,
    #[pyo3(get, set)]
    message: String,
}
fn peer_id_from_str(val: &str) -> PeerId {
    let bytes = bs58::decode(val).into_vec().unwrap();
    let peer_id = PeerId::from_bytes(&bytes).unwrap();
    peer_id
}
