use libp2p::{
    futures::StreamExt,
    gossipsub, mdns, noise,
    swarm::{NetworkBehaviour, SwarmEvent},
    tcp, yamux, PeerId, Swarm, Multiaddr, request_response::ResponseChannel,
};
use pyo3::prelude::*;
use serde::{Deserialize, Serialize};
use std::{
    collections::{hash_map::DefaultHasher, HashSet},
    error::Error,
    hash::{Hash, Hasher},
    sync::{Arc, Mutex},
    thread,
    time::{self, Duration},
};
use tokio::io;
use tracing_subscriber::EnvFilter;
use async_std::task::spawn;
use futures::channel::{mpsc, oneshot};
use futures::prelude::*;

#[derive(NetworkBehaviour)]
pub struct MyBehaviour {
    pub gossipsub: gossipsub::Behaviour,
    mdns: mdns::tokio::Behaviour,
}

#[derive(Clone)]
#[pyclass(name = "Node", subclass)]
pub struct Node {
    pub id: PeerId,
    pub message: String,
    pub peers_id: HashSet<PeerId>,
    pub swarm: Arc<tokio::sync::Mutex<Swarm<MyBehaviour>>>,
    pub topic: gossipsub::IdentTopic,
}

#[derive(Clone)]
pub(crate) struct Client {
    sender: mpsc::Sender<Command>,
}

#[pymodule]
fn pyo3_example(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<Node>()?;
    Ok(())
}

impl Client {
    /// Listen for incoming connections on the given address.
    pub(crate) async fn start_listening(
        &mut self,
        addr: Multiaddr,
    ) -> Result<(), Box<dyn Error + Send>> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::StartListening { addr, sender })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.")
    }

    /// Dial the given peer at the given address.
    pub(crate) async fn dial(
        &mut self,
        peer_id: PeerId,
        peer_addr: Multiaddr,
    ) -> Result<(), Box<dyn Error + Send>> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::Dial {
                peer_id,
                peer_addr,
                sender,
            })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.")
    }

    /// Advertise the local node as the provider of the given file on the DHT.
    pub(crate) async fn start_providing(&mut self, file_name: String) {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::StartProviding { file_name, sender })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.");
    }

    /// Find the providers for the given file on the DHT.
    pub(crate) async fn get_providers(&mut self, file_name: String) -> HashSet<PeerId> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::GetProviders { file_name, sender })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not to be dropped.")
    }

    /// Request the content of the given file from the given peer.
    pub(crate) async fn request_file(
        &mut self,
        peer: PeerId,
        file_name: String,
    ) -> Result<Vec<u8>, Box<dyn Error + Send>> {
        let (sender, receiver) = oneshot::channel();
        self.sender
            .send(Command::RequestFile {
                file_name,
                peer,
                sender,
            })
            .await
            .expect("Command receiver not to be dropped.");
        receiver.await.expect("Sender not be dropped.")
    }

    /// Respond with the provided file content to the given request.
    pub(crate) async fn respond_file(
        &mut self,
        file: Vec<u8>,
        channel: ResponseChannel<FileResponse>,
    ) {
        self.sender
            .send(Command::RespondFile { file, channel })
            .await
            .expect("Command receiver not to be dropped.");
    }
}

impl Node {
    async fn start(self) -> PyResult<()> {
        let mut node = self;
        async_run(&mut node).await;
        // spawn(async_run(&mut node));
        Ok(())
    }
}

#[pymethods]
impl Node {
    #[new]
    pub fn new(
        address: Option<&str>,
        port: Option<u32>,
        tcp: Option<bool>,
        udp: Option<bool>,
        msg_topic: Option<&str>,
    ) -> Self {
        let rt = tokio::runtime::Runtime::new().unwrap();
        let node = rt.block_on(async {
            create_node(address, port, tcp, udp, msg_topic)
                .await
                .unwrap()
        });
        node
    }

    #[getter]
    fn get_id(&self) -> PyResult<String> {
        Ok(self.id.to_string())
    }

    ///----------------------ERROR--------------------
    // fn run(&mut self) -> PyResult<()> {
    //     let rt = tokio::runtime::Runtime::new().unwrap();
    //     rt.block_on(async {
    //         async_run(self).await;
    //     });
    //     Ok(())
    // }

    fn run<'a>(&self, py: Python<'a>) -> PyResult<&'a PyAny>{
        // let mut node_clone = self.clone();
        // let arc_node = Arc::new(Mutex::new(node_clone));
        // let node_arc_clone = Arc::clone(&arc_node);
        // let mut node_guard = (&node_arc_clone).lock().unwrap();
        // let node = &mut *node_guard;
        let node = self.clone();
        pyo3_asyncio::tokio::future_into_py(py, node.start())
    }
    // fn run<'a>(slf: Py<Self>, py: Python<'a>) -> PyResult<()> {
    //     // let mut node_clone = self.clone();
    //     // let arc_node = Arc::new(Mutex::new(node_clone));
    //     // let node_arc_clone = Arc::clone(&arc_node);
    //     // let mut node_guard = (&node_arc_clone).lock().unwrap();
    //     // let node = &mut *node_guard;
    //     // let node = self.clone();
    //     // pyo3_asyncio::tokio::future_into_py(py, node.start())
    //     // pyo3_asyncio::tokio::future_into_py(py, async move { Node::start(&slf).await })
    //     Ok(())
    // }
    // fn run(slf: Py<Self>, py: Python<'_>) {
    //     let cloned = slf.clone_ref(py);
    //     thread::spawn(move || {
    //         println!("Wait 10 seconds before print informations....");
    //         // let sleep_time = time::Duration::from_millis(10000);
    //         // thread::sleep(sleep_time);
    //         Python::with_gil(|py| {
    //             let mut slf = cloned.borrow_mut(py);
    //             let rt = tokio::runtime::Runtime::new().unwrap();
    //             rt.block_on(async {
    //                 async_run(&mut slf).await;
    //                 tokio::spawn(async {async_run(&mut slf).await});
    //             });
    //         });
    //     });
    // }

    fn handle_list_peers(&mut self) -> PyResult<()> {
        println!("Discovered Peers:");
        let rt = tokio::runtime::Runtime::new().unwrap();
        rt.block_on(async {
            let swarm = self.swarm.lock().await;
            let nodes = swarm.behaviour().mdns.discovered_nodes();
            let mut unique_peers = HashSet::new();
            for peer in nodes {
                unique_peers.insert(peer);
                self.peers_id.insert(*peer);
            }
            unique_peers.iter().for_each(|p| println!("{}", p));
        });

        // let rt = tokio::runtime::Runtime::new().unwrap();
        // rt.block_on(async { async_handle_list_peers(self).await });
        Ok(())
    }
}

async fn create_node(
    address: Option<&str>,
    port: Option<u32>,
    tcp: Option<bool>,
    udp: Option<bool>,
    msg_topic: Option<&str>,
) -> Result<Node, Box<dyn Error>> {
    let addr: &str = match address {
        Some(val) => val,
        None => "0.0.0.0",
    };

    let po = match port {
        Some(val) => val,
        None => 0,
    };

    let _ = tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .try_init();

    let mut swarm = libp2p::SwarmBuilder::with_new_identity()
        // .with_tokio()
        .with_tokio()
        .with_tcp(
            tcp::Config::default().port_reuse(true).nodelay(true),
            noise::Config::new,
            yamux::Config::default,
        )
        .unwrap()
        .with_quic()
        .with_behaviour(|key| {
            // To content-address message, we can take the hash of message and use it as an ID.
            let message_id_fn = |message: &gossipsub::Message| {
                let mut s = DefaultHasher::new();
                message.data.hash(&mut s);
                gossipsub::MessageId::from(s.finish().to_string())
            };

            // Set a custom gossipsub configuration
            let gossipsub_config = gossipsub::ConfigBuilder::default()
                .heartbeat_interval(Duration::from_secs(10)) // This is set to aid debugging by not cluttering the log space
                .validation_mode(gossipsub::ValidationMode::Strict) // This sets the kind of message validation. The default is Strict (enforce message signing)
                .message_id_fn(message_id_fn) // content-address messages. No two messages of the same content will be propagated.
                .build()
                .map_err(|msg| io::Error::new(io::ErrorKind::Other, msg))?; // Temporary hack because `build` does not return a proper `std::error::Error`.

            // build a gossipsub network behaviour
            let gossipsub = gossipsub::Behaviour::new(
                gossipsub::MessageAuthenticity::Signed(key.clone()),
                gossipsub_config,
            )?;

            let mdns =
                mdns::tokio::Behaviour::new(mdns::Config::default(), key.public().to_peer_id())?;
            Ok(MyBehaviour { gossipsub, mdns })
        })
        .unwrap()
        .with_swarm_config(|c| c.with_idle_connection_timeout(Duration::from_secs(60)))
        .build();

    // Create a Gossipsub topic
    let topic = gossipsub::IdentTopic::new(msg_topic.unwrap());
    // subscribes to our topic
    swarm.behaviour_mut().gossipsub.subscribe(&topic).unwrap();

    // Listen on all interfaces and whatever port the OS assigns
    if tcp.unwrap() {
        let tcp_address = format!("/ip4/{}/tcp/{}", addr, po);
        swarm
            .listen_on(tcp_address.to_owned().parse().unwrap())
            .unwrap();
    }
    if udp.unwrap() {
        let udp_address = format!("/ip4/{}/udp/{}/quic-v1", addr, po);
        swarm
            .listen_on(udp_address.to_owned().parse().unwrap())
            .unwrap();
    } else if !tcp.unwrap() {
        let tcp_address = format!("/ip4/{}/tcp/{}", addr, po);
        swarm
            .listen_on(tcp_address.to_owned().parse().unwrap())
            .unwrap();
    }

    println!("BEFORE: Local node is listenning on :");
    swarm.listeners().for_each(|f| println!("{}", f));

    let id = *swarm.local_peer_id();

    println!("AFTER: Local node is listenning on :");
    swarm.listeners().for_each(|f| println!("{}", f));

    Ok(Node {
        id,
        message: String::new(),
        peers_id: HashSet::new(),
        swarm: Arc::new(tokio::sync::Mutex::new(swarm)),
        topic,
    })
}

async fn async_run_test<'a>(nod: &'static Py<Node>) -> PyResult<()> {
    Python::with_gil(|py| {
        let t = unsafe { py.new_pool() };
        let mut node = nod.try_borrow_mut(py).unwrap();
        // let swarm_arc_clone = Arc::clone(&node.swarm);
        // // let mut swarm_guard = swarm_arc_clone.lock().await;
        // let swarm = &mut *swarm_guard;
        let p = t.python();

        // pyo3_asyncio::tokio::future_into_py(p, async_run(&mut node));
    });
    Ok(())
}

pub async fn async_run(node: &mut Node) -> PyResult<()> {
    let swarm_arc_clone = Arc::clone(&node.swarm);
    let mut swarm_guard = swarm_arc_clone.lock().await;
    let swarm = &mut *swarm_guard;
    loop {
        // let evt = {
        futures::select! {
            event = swarm.next() => handle_event(node, event.expect("Swarm stream to be infinite.")).await,
        };
    }
}

async fn handle_event(node: &mut Node, event: SwarmEvent<MyBehaviourEvent>) {
    match event {
        SwarmEvent::Behaviour(MyBehaviourEvent::Gossipsub(gossipsub::Event::Message {
            propagation_source: peer_id,
            message_id: id,
            message,
        })) => {
            println!(
                "Got message: '{}' with id: {id} from peer: {peer_id}",
                String::from_utf8_lossy(&message.data),
            );

            node.message = String::from_utf8_lossy(&message.data).to_string();
            // None
        }
        SwarmEvent::Behaviour(MyBehaviourEvent::Mdns(mdns::Event::Discovered(list))) => {
            for (peer_id, _multiaddr) in list {
                println!("mDNS discover peer has discovered: {}", peer_id);
                node.swarm
                    .lock()
                    .await
                    .behaviour_mut()
                    .gossipsub
                    .add_explicit_peer(&peer_id);
            }
            // None
        }
        SwarmEvent::Behaviour(MyBehaviourEvent::Mdns(mdns::Event::Expired(list))) => {
            for (peer_id, _multiaddr) in list {
                println!("mDNS discover peer has expired: {peer_id}");
                node.swarm
                    .lock()
                    .await
                    .behaviour_mut()
                    .gossipsub
                    .remove_explicit_peer(&peer_id);
            }
            // None
        }
        SwarmEvent::ConnectionEstablished {
            peer_id,
            connection_id,
            endpoint,
            num_established,
            concurrent_dial_errors,
            established_in,
        } => todo!(),
        SwarmEvent::ConnectionClosed {
            peer_id,
            connection_id,
            endpoint,
            num_established,
            cause,
        } => todo!(),
        SwarmEvent::IncomingConnection {
            connection_id,
            local_addr,
            send_back_addr,
        } => todo!(),
        SwarmEvent::IncomingConnectionError {
            connection_id,
            local_addr,
            send_back_addr,
            error,
        } => todo!(),
        SwarmEvent::OutgoingConnectionError {
            connection_id,
            peer_id,
            error,
        } => todo!(),
        SwarmEvent::NewListenAddr {
            listener_id,
            address,
        } => todo!(),
        SwarmEvent::ExpiredListenAddr {
            listener_id,
            address,
        } => todo!(),
        SwarmEvent::ListenerClosed {
            listener_id,
            addresses,
            reason,
        } => todo!(),
        SwarmEvent::ListenerError { listener_id, error } => todo!(),
        SwarmEvent::Dialing {
            peer_id,
            connection_id,
        } => todo!(),
        SwarmEvent::NewExternalAddrCandidate { address } => todo!(),
        SwarmEvent::ExternalAddrConfirmed { address } => todo!(),
        SwarmEvent::ExternalAddrExpired { address } => todo!(),
        _ => todo!(),
    }
}

#[derive(Debug)]
enum Command {
    StartListening {
        addr: Multiaddr,
        sender: oneshot::Sender<Result<(), Box<dyn Error + Send>>>,
    },
    Dial {
        peer_id: PeerId,
        peer_addr: Multiaddr,
        sender: oneshot::Sender<Result<(), Box<dyn Error + Send>>>,
    },
    StartProviding {
        file_name: String,
        sender: oneshot::Sender<()>,
    },
    GetProviders {
        file_name: String,
        sender: oneshot::Sender<HashSet<PeerId>>,
    },
    RequestFile {
        file_name: String,
        peer: PeerId,
        sender: oneshot::Sender<Result<Vec<u8>, Box<dyn Error + Send>>>,
    },
    RespondFile {
        file: Vec<u8>,
        channel: ResponseChannel<FileResponse>,
    },
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
struct FileRequest(String);
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub(crate) struct FileResponse(Vec<u8>);
// #[pyfunction]
// pub fn frun<'a>(node: &mut Node, py: Python<'a>) -> PyResult<&'a PyAny> {
//     pyo3_asyncio::tokio::future_into_py(py, async_run(node))
//     // Ok(())
// }
